﻿using UnityEngine;

public class Joystick : MonoBehaviour
{
    Vector2 startPoint, endPoint, initialPosition;
    bool touchStart = false;
    public Transform Player;
    public float speed = 0.3f;

    private void Start()
    {
        initialPosition = transform.position;
    }

    private void OnMouseDown()
    {
        startPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    private void OnMouseDrag()
    {
        touchStart = true;
        endPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
    }

    private void OnMouseUp()
    {
        touchStart = false;
    }

    private void Update()
    {
        if (touchStart)
        {
            Vector2 offset = endPoint - startPoint;
            Vector2 direction = Vector2.ClampMagnitude(offset, 1f);
            MovePlayer(direction);
            transform.position = new Vector2(startPoint.x + direction.x, startPoint.y + direction.y);
        }

        else
        {
            transform.position = initialPosition;
        }
    }

    void MovePlayer(Vector2 dir)
    {
        Player.Translate(dir * Time.deltaTime * speed);
    }


}

