﻿using UnityEngine;
using UnityEngine.UI;

public class GPSText : MonoBehaviour
{
    public Text coordinates;

    private void Update()
    {
        coordinates.text = "Lat : " + GPS.Instance.latitude.ToString() + "Lon : " + GPS.Instance.longitude.ToString();

    }
}
