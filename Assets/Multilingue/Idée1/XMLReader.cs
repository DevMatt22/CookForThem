﻿using System.Collections.Generic;
using System.Xml;
using UnityEngine;
using UnityEngine.UI;

public class XMLReader : MonoBehaviour
{
    public TextAsset dictionnary;
    public string language_name;
    public int currentLanguage;

    private string bonjour;
    private string aurevoir;

    //  Les 3 variables suivantes sont pour le UI
    public Text TextBonjour;

    public Text TextAuRevoir;
    public Dropdown select;
    private readonly List<Dictionary<string, string>> Languages = new List<Dictionary<string, string>>();
    private Dictionary<string, string> obj;

    private void Awake()
    {
        Reader();
    }

    // Update is called once per frame
    private void Update()
    {
        Languages[currentLanguage].TryGetValue("name", out language_name);
        Languages[currentLanguage].TryGetValue("bonjour", out bonjour);
        Languages[currentLanguage].TryGetValue("aurevoir", out aurevoir);

        TextBonjour.text = bonjour;
        TextAuRevoir.text = aurevoir;
    }

    private void Reader()
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(dictionnary.text);
        XmlNodeList LanguageList = xmlDoc.GetElementsByTagName("language");

        foreach (XmlNode Languagevalue in LanguageList)
        {
            XmlNodeList languageContent = Languagevalue.ChildNodes;
            obj = new Dictionary<string, string>();

            foreach (XmlNode value in languageContent)
            {
                if (value.Name == "name")
                {
                    obj.Add(value.Name, value.InnerText);
                }

                if (value.Name == "bonjour")
                {
                    obj.Add(value.Name, value.InnerText);
                }

                if (value.Name == "aurevoir")
                {
                    obj.Add(value.Name, value.InnerText);
                }
            }

            Languages.Add(obj);
        }
    }

    public void ValueChangecheck()
    {
        currentLanguage = select.value;
    }
}