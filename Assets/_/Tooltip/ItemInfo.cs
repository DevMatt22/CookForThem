﻿using UnityEngine;

public class ItemInfo : MonoBehaviour
{

    public Sprite sprite;
    public string itemName;
    public string itemDescription;
    public int STR;
    public int DEX;
    public int CON;

}
