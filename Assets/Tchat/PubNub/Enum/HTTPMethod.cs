namespace PubNubAPI
{
    public enum HTTPMethod
    {
        Get,
        Post,
        Delete,
        Patch
    }
}

