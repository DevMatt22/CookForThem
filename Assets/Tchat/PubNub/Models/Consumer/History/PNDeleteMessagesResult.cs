namespace PubNubAPI
{
    public class PNDeleteMessagesResult : PNResult
    {
        public string Message { get; set; }
    }
}