namespace PubNubAPI
{
    public class PNChannelGroupsRemoveChannelResult : PNResult
    {
        public string Message { get; set; }

    }

}