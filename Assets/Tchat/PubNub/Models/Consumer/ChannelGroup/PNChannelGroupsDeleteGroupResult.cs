namespace PubNubAPI
{
    public class PNChannelGroupsDeleteGroupResult : PNResult
    {
        public string Message { get; set; }
    }

}