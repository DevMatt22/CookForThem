using System;
using System.Collections.Generic;

namespace PubNubAPI
{
    public class WhereNowBuilder
    {
        private readonly WhereNowRequestBuilder pubBuilder;

        public WhereNowBuilder(PubNubUnity pn)
        {
            pubBuilder = new WhereNowRequestBuilder(pn);
        }
        public WhereNowBuilder Uuid(string uuidForWhereNow)
        {
            pubBuilder.Uuid(uuidForWhereNow);
            return this;
        }

        public WhereNowBuilder QueryParam(Dictionary<string, string> queryParam)
        {
            pubBuilder.QueryParam(queryParam);
            return this;
        }

        public void Async(Action<PNWhereNowResult, PNStatus> callback)
        {
            pubBuilder.Async(callback);
        }
    }
}